<!DOCTYPE html>
<html lang="es">
<head>
	<title>Solicitud de Empleo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="shortcut icon" href="img/eficasia.ico">
	<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="lib/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<header style="background: #eee;">
			<div class="container">
				<!-- <div class="col-lg-6" style="display: inline;"> -->
				<img src="img/eficasia.png" class="img_logo">
				<!-- </div> -->
				<!-- <div class="col-lg-6 inline" style=""> -->
				<h1 class="h1_title">Solicitud de Empleo</h1>
				<h4>CAPITAL HUMANO</h4>
				<!-- </div> -->
			</div>
		</header>
		<section class="sect_wizard">
			<div class="stepwizard col-md-12">
				<div class="stepwizard-row setup-panel">
					<div class="stepwizard-step">
						<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
						<p>Datos Solicitud</p> <!-- Personales -->
					</div>
					<div class="stepwizard-step">
						<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
						<p>Datos Personales</p>
					</div>
					<div class="stepwizard-step">
						<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
						<p>Domicilio</p>
					</div>
					<div class="stepwizard-step">
						<a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
						<p>Datos Familiares</p>
					</div>
					<div class="stepwizard-step">
						<a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
						<p>Preguntas Generales</p>
					</div>
					<div class="stepwizard-step">
						<a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
						<p>Datos Escolares</p>
					</div>
					<div class="stepwizard-step">
						<a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
						<p>Experiencia Laboral</p>
					</div>
				</div>
			</div>

			<form role="form" class="form-well" action="" method="post" data-toggle="validator">
				<div class="row setup-content" id="step-1">
					<div class="col-xs-12 col-md-12">
						<div class="col-md-12">
							<h3 class="form_title">Datos Solicitud</h3> 
							<div class="form-group">
								<div class="form-inline row">
									<div class="form-group col-md-6">
										<label>Puesto Solicitado</label> <!-- class="control-label" -->
										<select class="form-control" id="puestos">
											<option value="0">Selecciona una opción</option>
											<option value="1">Vacante 1</option>
											<option value="2">Vacante 2</option>
										</select>
										<!-- <div class="help-block with-errors">Responde el campo</div> -->
									</div>
									<div class="form-group col-md-6">
										<label>¿Cómo se enteró de la vacante?</label> <!-- class="control-label" -->
										<select class="form-control">
											<option value="">Medio 1</option>
											<option value="">Medio 2</option>
										</select>
										<!-- <div class="help-block with-errors">Responde el campo</div> -->
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="form-inline row">
									<div class="form-group col-md-6">
										<label>Disponibilidad en fines de semana</label>
										<select class="form-control">
											<option value="">Sí</option>
											<option value="">No</option>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label class="">Sueldo Deseado</label>
										<select class="form-control">
											<option>$1500 - $2000</option>
											<option>$2000 - $2500</option>
											<option>$2500 - $3000</option>
											<option>$3500 - $4000</option>
										</select>
									</div>
								</div>
							</div>

			        <button class="btn btn-primary nextBtn btn-md pull-right" type="button" >Siguiente</button>
			      </div>
			  </div>
			</div>
			<div class="row setup-content" id="step-2">
				<div class="col-xs-12 col-md-12">
					<div class="col-md-12">
						<h3 class="form_title">Datos Personales</h3>
						<div class="form-group"> 
							<div class="form-inline row">
								<div class="form-group col-md-6 has-error has-danger">
									<label class="" name="" pattern="^[0-9]{11,}$" placeholder="Ingresa únicamente los primeros 10 dígitos de tu NSS" maxlength="11" minlength="9" type="text" autofocus required >CURP</label>
									<input class="form-control" type="text" name="">
									<button class="btn btn-md btn-info btn_curp">Buscar</button>
								</div>
								<div class="form-group col-md-6"> 
									<label class="hide_nss">NSS</label>
									<input class="form-control hide_nss" type="" name="">
								</div>
							</div>
						</div>		

						<div class="form-group">
							<div class="form-inline row">
								<div class="form-group col-md-4">
									<label class="hide_paterno">Apellido Paterno</label>
									<input class="form-control hide_paterno" type="" name="">
								</div>
								<div class="form-group col-md-4">
									<label class="hide_materno">Apellido Materno</label>
									<input class="form-control hide_materno" type="" name="">
								</div>
								<div class="form-group col-md-4">
									<label class="hide_nombre">Nombre(s)</label>
									<input class="form-control hide_nombre" type="" name="">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="form-inline row">
								<div class="form-group col-md-4">
									<label class="hide_sexo">Sexo</label>
									<input class="form-control hide_sexo" type="" name="">
								</div>
								<div class="form-group col-md-4">
									<label class="hide_fecha">Fecha de nacimiento</label>
									<input type="date" class="hide_fecha" name="">
								</div>
								<div class="form-group col-md-4">
									<label class="hide_civil">Estado Civil</label>
									<select class="form-control hide_civil">
										<option>Soltero(a)</option>
										<option>Casado(a)</option>
										<option>Unión libre</option>
										<option>Viudo(a)</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="form-inline row">
								<div class="form-group col-md-6 hide_credito">
									<label class="radio_rgt">¿Tiene tarjeta(s) de crédito?</label>
									<input type="radio" value="" name="tarjeta" class="mostr-elem-tarj"> Sí
									<input type="radio" value="" name="tarjeta" class="ocult-elem-tarj"> No
								</div>
								<div class="form-group col-md-6">
									<label class="Elemt-tarjeta">¿Cuáles?</label> <!-- Dependiente sí -->
									<select class="form-control Elemt-tarjeta"> <!-- Dependiente sí -->
										<option value="">Santander</option><!-- Dependiente sí -->
										<option value="">Bancomer</option><!-- Dependiente sí -->
										<option value="">Banamex</option><!-- Dependiente sí -->
										<option value="">Scotiabank</option><!-- Dependiente sí -->
									</select>
								</div>
							</div>
						</div>
			        <button class="btn btn-primary nextBtn btn-md pull-right nxt_personales" type="button" >Siguiente</button>
			    </div>
			</div>
		</div>
		<div class="row setup-content" id="step-3">
			<div class="col-xs-12 col-md-12">
				<div class="col-md-12">
					<h3 class="form_title">Domicilio</h3>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label>No. Interno</label>
								<input  class="form-control" type="" name="">
							</div>
							<div class="form-group col-md-4">
								<label>Calle</label>
								<input  class="form-control" type="text">
							</div>
							<div class="form-group col-md-4">
								<label>No. Externo</label>
								<input  class="form-control" type="" name="">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-3 has-error has-danger">
								<label>C.P.</label>  <!-- Validar -->
								<input  class="form-control" type="" name="" pattern="[0-9]{5}" title="Utiliza un formato de 5 digitos"/>
							</div>
							<div class="form-group col-md-3">
								<label>Colonia</label> <!-- Depende de CP -->
								<select class="form-control" disabled>  <!-- Depende de CP -->
									<option value="">Col. 1</option>  <!-- Depende de CP -->
									<option value="">Col. 2</option>  <!-- Depende de CP -->
								</select>  <!-- Depende de CP -->
							</div>
							<div class="form-group col-md-3">
								<label>Delegación</label>  <!-- Depende de CP -->
								<select class="form-control" disabled>  <!-- Depende de CP -->
									<option value="">Tlalpan</option>  <!-- Depende de CP -->
									<option value="">Iztapalapa</option>  <!-- Depende de CP -->
								</select>  <!-- Depende de CP -->
							</div>
							<div class="form-group col-md-3">
								<label>Estado</label>  <!-- Depende de CP -->
								<select class="form-control" disabled>  <!-- Depende de CP -->
									<option value="">Puebla</option>  <!-- Depende de CP -->
									<option value="">Veracruz</option>  <!-- Depende de CP -->
								</select>  <!-- Depende de CP -->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-3">
								<label for="cel_lab">Celular</label> 
								<input class="form-control" type="" name="" required="required" pattern="[0-9]{10}" title="Utiliza un formato de 10 digitos" id="cel_lab">
							</div>
							<div class="form-group col-md-3">
								<label>Tel. de casa</label> 
								<input class="form-control" type="" name="" pattern="[0-9]{8}" title="Utiliza un formato de 8 digitos">
							</div>
							<div class="form-group col-md-3">
								<label>Tel. de recados</label> 
								<input class="form-control" type="" name="" pattern="[0-9]{8}" title="Utiliza un formato de 10 digitos">
							</div>
							<div class="form-group col-md-3">
								<label>Correo electrónico</label> 
								<input class="form-control" type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Utiliza un formato de: caracteres@caracteres.dominio">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label>Traslado al trabajo</label>
								<select class="form-control">
									<option>30 min - 1 hr</option>
									<option>1 hr - 1:30 hr</option>
									<option>1:30 hr - 2:00 hr</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Propia / Rentada</label>
								<select class="form-control">
									<option value="">Propia</option>
									<option value="">Rentada</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>¿Cuántas personas viven en su casa?</label>
								<select class="form-control">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-6">
								<label for="">Número de personas que contribuyen al gasto familiar</label>
								<select class="form-control">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label class="radio_rgt">¿Tiene usted dependiente(s) económico(s)?</label>
								<input type="radio" value="1" name=""> Sí
								<input type="radio" value="2" name=""> No
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-12">
								<label>¿Quiénes?</label> <!-- Depende de sí  -->
								<input class="form-control" type="" name="">
							</div>
						</div>
					</div>
					
					<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
				</div>
			</div>
		</div>
		<div class="row setup-content" id="step-4">
			<div class="col-xs-12 col-md-12">
				<div class="col-md-12">
					<h3 class="form_title">Datos Familiares</h3>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">Padre</label>
								<label class="radio_rgt">¿Vive?</label>
								<input type="checkbox" name="" value="" class="mostr-elem-pad checkd"> Sí
								<input type="checkbox" name="" value="" class="ocul-elem-pad checkd"> No
							</div>
							<div class="form-group col-md-4">
								<label class='Elemt-pad'>Nombre</label> <!-- Dependiente sí-->
								<input class="form-control Elemt-pad" type="" name=""> <!-- Dependiente sí-->
							</div>
							<div class="form-group col-md-4">
								<label class="Elemt-pad">Ocupación</label>
								<select class="form-control Elemt-pad"> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente sí-->
								</select> <!-- Dependiente sí-->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">Madre</label>
								<label class="radio_rgt">¿Vive?</label>
								<input type="checkbox" name="" value="" class="mostr-elem-Mad checkd2"> Sí
								<input type="checkbox" name="" value="" class="ocul-elem-Mad checkd2"> No
							</div>
							<div class="form-group col-md-4">
								<label class='Elemt-Mad'>Nombre</label> <!-- Dependiente sí-->
								<input class="form-control Elemt-Mad" type="" name=""> <!-- Dependiente sí-->
							</div>
							<div class="form-group col-md-4">
								<label class="Elemt-Mad">Ocupación</label>
								<select class="form-control Elemt-Mad"> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente sí-->
								</select> <!-- Dependiente sí-->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">Hermanos(as)</label>
								<input type="checkbox" value="" name="Elem-herm" class='mostr-elem-herm'> Sí
								<input type="checkbox" value="" name="Elem-herm" class='ocul-elem-herm'> No
							</div>
							<div class="form-group col-md-4">
								<label class='Elem-herm'>¿Cuántos?</label> <!-- Dependiente sí -->
								<select name="" id="Cantidad" class='Elem-herm'> <!-- Dependiente sí -->
									<option value="">0</option> <!-- Dependiente sí -->
									<option value="">1</option> <!-- Dependiente sí -->
									<option value="">2</option> <!-- Dependiente sí -->
									<option value="">3</option> <!-- Dependiente sí -->
									<option value="">4</option> <!-- Dependiente sí -->
									<option value="">5</option> <!-- Dependiente sí -->
									<option value="">6</option> <!-- Dependiente sí -->
									<option value="">7</option> <!-- Dependiente sí -->
									<option value="">8</option> <!-- Dependiente sí -->
									<option value="">9</option> <!-- Dependiente sí -->
								</select> <!-- Dependiente sí -->
							</div>
						</div>
					</div>

					<!-- Test hermanos -->
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive1 -->
								<div id="vive-herm1">		
						          	<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
						          	<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
						          	<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive' id="">Nombre</label> <!-- Dependiente SÍ VIVE-->
					          	<input class="form-control" type="" name="" class='nombre-vive'> <!-- Dependiente SÍ VIVE-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive2 -->
								<div id="vive-herm2">		
						          	<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
						          	<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
						          	<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>
								
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive3 -->
								<div id="vive-herm3">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive4 -->
								<div id="vive-herm4">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive5 -->
								<div id="vive-herm5">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
			          			<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive6 -->
								<div id="vive-herm6">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive7 -->
								<div id="vive-herm7">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive8 -->
								<div id="vive-herm8">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<!-- vive9 -->
								<div id="vive-herm9">		
									<label>¿Vive?</label>   <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>Sí</span> <!-- Dependiente # de herman@s -->
									<input type="radio" value="" name="vive"> <span id='id-vive'>No</span><!-- Dependiente # de herman@s -->
								</div>
							</div>
							<div class="form-group col-md-4">
								<label class='nombre-vive'>Nombre</label> <!-- Dependiente SÍ VIVE!!!-->
								<input type="" name="" class='nombre-vive form-control'> <!-- Dependiente SÍ VIVE!!!-->
							</div>
							<div class="form-group col-md-4">
								<label>Ocupación</label>
								<select class='nombre-vive form-control'> <!-- Dependiente sí-->
									<option>Ocupación</option> <!-- Dependiente SÍ VIVE!-->
								</select> <!-- Dependiente SÍ VIVE-->
							</div>
						</div>
					</div>

					<!-- /test hermanos -->




						
					<!-- <div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<div id="vive-herm1">
									<label class="radio_rgt">¿Vive?</label>   Dependiente # de herman@s 
									<input type="checkbox" value="" name=""> <span id='id-vive'>Sí</span>  Dependiente # de herman@s 
									<input type="checkbox" value="" name=""> <span id='id-vive'>No</span>Dependiente # de herman@s 
								</div>
								
							</div>
							<div class="form-group col-md-4">
								<label>Nombre</label>  Dependiente SÍ VIVE!!!
								<input class="form-control" type="" name="">  Dependiente SÍ VIVE!!!
							</div>
							<div class="form-group col-md-4">
							<label>Ocupación</label>
								<select class="form-control"> Dependiente sí
									<option>Ocupación</option> Dependiente SÍ VIVE!!!
								</select>  Dependiente SÍ VIVE!!!
							</div>
						</div>
					</div> -->
					<div class="form-group">
						<div  class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">¿Tiene hijos(as)?</label>
								<input type="checkbox" value="" name=""> Sí 
								<input type="checkbox" value="" name=""> No
							</div>
							<div class="form-group col-md-4">
								<label>¿Cuántos hijos(as) tiene?</label>
								<select class="form-control"> <!-- Dependiente sí -->
									<option value="">0</option> <!-- Dependiente sí -->
									<option value="">1</option> <!-- Dependiente sí -->
									<option value="">2</option> <!-- Dependiente sí -->
									<option value="">3</option> <!-- Dependiente sí -->
									<option value="">4</option> <!-- Dependiente sí -->
									<option value="">5</option> <!-- Dependiente sí -->
									<option value="">6</option> <!-- Dependiente sí -->
									<option value="">7</option> <!-- Dependiente sí -->
									<option value="">8</option> <!-- Dependiente sí -->
									<option value="">9</option> <!-- Dependiente sí -->
								</select> <!-- Dependiente sí -->
							</div>
						</div>
						
					</div>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
			</div>
		</div>
	</div>
	<div class="row setup-content" id="step-5">
		<div class="col-xs-6 col-md-12">
			<div class="col-md-12">
				<h3 class="form_title">Preguntas Generales</h3>
					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">¿Ha trabajado en Eficasia?</label>
								<input type="radio" value="" name=""> Sí
								<input type="radio" value="" name=""> No
							</div>
							<div class="form-group col-md-4">
								<label>Área / Campaña</label>  <!-- Dependiente sí-->
								<input class="form-control" type="" name="">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label>¿Tiene o ha tenido familiares trabajando en Eficasia?</label>
								<input type="radio" value="" name=""> Sí
								<input type="radio" value="" name=""> No
							</div>
							<div class="form-group col-md-4">
								<label>Nombre</label> <!-- Dependiente (sí) -->
								<input class="form-control" type="" name=""> <!-- Dependiente (sí) -->
							</div>
							<div class="form-group col-md-4">
								<label>Área / Campaña</label> <!-- Dependiente (sí) -->
								<input class="form-control" type="" name=""> <!-- Dependiente (sí) -->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">¿Ha trabajado en IKÉ?</label>
								<input type="radio" value="" name=""> Sí
								<input type="radio" value="" name=""> No
							</div>
							<div class="form-group col-md-4">
								<label>Nombre</label> <!-- Dependiente (sí) -->
								<input class="form-control" type="" name=""> <!-- Dependiente (sí) -->
							</div>
							<div class="form-group col-md-4">
								<label>Área / Campaña</label> <!-- Dependiente (sí) -->
								<input class="form-control" type="" name=""> <!-- Dependiente (sí) -->
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">¿Cuenta con Crédito INFONAVIT?</label>
								<input type="radio" value="" name=""> Sí
								<input type="radio" value="" name=""> No
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="form-inline row">
							<div class="form-group col-md-4">
								<label class="radio_rgt">¿Es referido(a)?</label>
								<input type="radio" value="" name=""> Sí
								<input class="form-control" type="radio" value="" name=""> No
							</div>
							<div class="form-group col-md-4">
								<label>Nombre de quien le refiere</label> <!-- Dependiente (sí) -->
								<input class="form-control" type="" name="">
							</div>
							<div class="form-group col-md-4">
								<label>Área / Campaña</label>
								<input class="form-control" type="" name="">	
							</div>
						</div>
					</div>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
			</div>
		</div>
	</div>
	<div class="row setup-content" id="step-6">
		<div class="col-xs-12 col-md-12">
			<div class="col-md-12">
				<h3 class="form_title">Datos Escolares</h3>
				<div class="form-group">
					<div class="form-inline row">
						<div class="form-group col-md-6">
							<label for="instituto">Institución</label>
							<input class="form-control" type="text" id="instituto" name="instituto">	
						</div>
						<div class="form-group col-md-6">
							<label>Grado máximo estudios</label>
							<select class="form-control">
								<option value="">Secundaria</option>
								<option value="">Medio Superior</option>
								<option value="">Superior</option>
								<option value="">Maestría</option>
								<option value="">Doctorado</option>
							</select>
						</div>
					</div>
				</div>
					
				<div class="form-group">
					<div class="form-inline row">
						<div class="form-group col-md-6">
							<label for="">Documento obtenido</label>
							<select class="form-control">
								<option value="">Certificado</option>
								<option value="">Título</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							<label for="">Carrera</label>
							<select class="form-control">
								<option value="">Ingeniería</option>
								<option value="">Lic. en Comunicaciones</option>
							</select>
						</div>
					</div>				
				</div>

				<div class="form-group">
					<div class="form-inline row">
						<div class="form-group col-md-6">
							<label class="radio_rgt">¿Estudia actualmente?</label>
							<input type="radio" value="" name=""> Sí
							<input type="radio" value="" name=""> No
						</div>
						<div class="form-group col-md-6">
							<label>Horario</label> <!-- Dependiente (sí) -->
							<select class="form-control"> <!-- Dependiente (sí) -->
								<option value="">Matutino</option> <!-- Dependiente (sí) -->
								<option value="">Vespetino</option> <!-- Dependiente (sí) -->
								<option value="">Nocturno</option> <!-- Dependiente (sí) -->
							</select> <!-- Dependiente (sí) -->
						</div>
					</div>
				</div>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
			</div>
		</div>
	</div>
	<div class="row setup-content" id="step-7">
		<div class="col-xs-12 col-md-12">
			<div class="col-md-12">
				<h3 class="form_title">Experiencia Laboral</h3>
				<div class="form-group">
					<div  class="form-inline row">
						<div class="form-group col-md-4">
							<label class="radio_rgt">¿Tiene experiencia laboral?</label>			
							<input type="radio"> Sí
							<input type="radio"> No	
						</div>
						<div class="form-group col-md-4">
							<label for="">¿Tiene experiencia en Call Center / Centro de Contacto?</label>  <!-- Dependiente (sí) -->
							<input type="radio"> Sí  <!-- Dependiente (sí) -->
							<input type="radio"> No  <!-- Dependiente (sí) -->
						</div>
						<div class="form-group col-md-4">
							<label class="radio_rgt">¿Tiene experiencia ventas?</label>  <!-- Dependiente (sí) -->
							<input type="radio"> Sí  <!-- Dependiente (sí) -->
							<input type="radio"> No  <!-- Dependiente (sí) -->
						</div>
					</div>
						
					<table class="table-striped">
						<thead>
							<tr>
								<th>DATOS</th>
								<th>ULTIMO</th>
								<th>ANTERIOR</th>
								<th>ANTERIOR</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Empresa</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Jefe directo</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Fecha de ingreso</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Fecha de salida</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Puesto</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Sueldo</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Bono o comisiones</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Motivo de separacion</td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>	
				<!-- <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button> -->
			</div>
			<!-- Large modal -->
			<button class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>

			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<p>Declaro bajo protesta de decir la verdad, que todos los contenidos en esta solicitud son veraces y me doy por enterado que cualquier omisión o falsedad, ya se intecional o no, es motivo para dar mi proceso de selección o incluso causar mi baja de la empresa ya estando laborando.</p>
						<p>Otorgo mi autorizacion a la empresa Eficasia y/o sus representantes para que mis datos sean enviados a otra empresas con un único fin de ser considerado para ofrecimiento laboral.</p>
						<div class="modal-footer">
							<button type="button" class="close" data-dismiss="modal">Sí</button>
							<button type="button" class="close" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /Large modal -->
		</div>
	</div>
</form>
</section>
</div>

<script type="text/javascript" src="lib/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="lib/tether-1.3.3/dist/js/tether.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="lib/validator/dist/validator.min.js"></script>
<script src=js/Main.js></script>
<!-- Steps -->
<script type="text/javascript">
	window.alert = function(){};
	var defaultCSS = document.getElementById('bootstrap-css');
	function changeCSS(css){
		if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
		else $('head > link').filter(':first').replaceWith(defaultCSS); 
	}
	$( document ).ready(function() {
	      //var iframe_height = parseInt($('html').height()); 
	      //window.parent.postMessage( iframe_height, 'https://bootsnipp.com');
	      $("input.checkd, input.checkd2").on("change", function() {
	          $("input.checkd, input.checkd").not(this).prop("checked", false);  
	      });
	  });

	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#myForm').validator();

			var navListItems = $('div.setup-panel div a'),
			allWells = $('.setup-content'),
			allNextBtn = $('.nextBtn');		

			allWells.hide();

			navListItems.click(function (e) {
				e.preventDefault();
				var $target = $($(this).attr('href')),
				$item = $(this);

				if (!$item.hasClass('disabled')) {
					navListItems.removeClass('btn-primary').addClass('btn-default');
					$item.addClass('btn-primary');
					allWells.hide();
					$target.show();
					$target.find('input:eq(0)').focus();
				}
			});

			allNextBtn.click(function(){
				var curStep = $(this).closest(".setup-content"),
				curStepBtn = curStep.attr("id"),
				nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
				curInputs = curStep.find("input[type='text'],input[type='url']"),
				isValid = true;

				$(".form-group").removeClass("has-error");
				for(var i=0; i<curInputs.length; i++){
					if (!curInputs[i].validity.valid){
						isValid = false;
						$(curInputs[i]).closest(".form-group").addClass("has-error");
					}
				}

				if (isValid)
					nextStepWizard.removeAttr('disabled').trigger('click');
			});

			$('div.setup-panel div a.btn-primary').trigger('click');

			$.post('core/catalogos.php', function(data, textStatus, xhr) {
				console.log(data);
			});



		});
	</script>
</body>
</html>